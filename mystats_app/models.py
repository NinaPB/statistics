from django.db import models
from datetime import datetime

# Create your models here.
class Action(models.Model):
    action_text = models.CharField(max_length = 200)
    entry_date = models.DateTimeField(default=datetime.now())
    
    def __str__(self):
            return self.action_text
    
    def entered(self):
        return self.entry_date
    