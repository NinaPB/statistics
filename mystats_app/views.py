from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime

import csv
# Create your views here.
from .models import Action

def index(request):
    actions = Action.objects.all()
    return render(request, 'mystats_app/index.html', {'actions': actions})
    print(request)
    
#Angelegt, um zu versuchen auf zweite html zuzugreifen: 
def added(request):
    action = request.POST['action_name']
    date = datetime.now()
    Action.objects.create(action_text = action) #siehe models
    getfile(action, date)
    return render(request, 'mystats_app/new_action.html')
    
def getfile(action, date):  
    datei = open('daten1.csv', 'a')
    datei.write(action)
    datei.write(';')
    datei.write(str(date))
    datei.write('\n')
    datei.close()
    #with open('daten.csv', 'w') as f:
    #    writer = csv.writer(f,quotechar ='"', quoting=csv.QUOTE_MINIMAL)
    #    writer.writerow(action)